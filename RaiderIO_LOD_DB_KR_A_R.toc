## Interface: 80205
## Author: cqwrteur
## Title: Raider.IO LOD Database KR Alliance Raid
## Version: @project-version@
## LoadOnDemand: 1
## X-RAIDER-IO-LOD: 2
## X-RAIDER-IO-LOD-FACTION: Alliance

db/db_raiding_kr_alliance_characters.lua
db/db_raiding_kr_alliance_lookup.lua
